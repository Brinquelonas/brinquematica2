
brinquematica npc 3.png
size: 2048,256
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: false
  xy: 904, 42
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 1442, 196
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: true
  xy: 340, 98
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 904, 106
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: false
  xy: 1090, 106
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body 1
  rotate: true
  xy: 1270, 178
  size: 76, 100
  orig: 76, 100
  offset: 0, 0
  index: -1
body 2
  rotate: true
  xy: 2, 6
  size: 248, 336
  orig: 248, 336
  offset: 0, 0
  index: -1
body 3
  rotate: false
  xy: 340, 52
  size: 224, 44
  orig: 224, 44
  offset: 0, 0
  index: -1
body_white mask
  rotate: true
  xy: 904, 68
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: false
  xy: 1270, 132
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 1026, 68
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: false
  xy: 340, 2
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: false
  xy: 438, 2
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
gola
  rotate: false
  xy: 566, 56
  size: 80, 40
  orig: 80, 40
  offset: 0, 0
  index: -1
gravata
  rotate: true
  xy: 1442, 214
  size: 40, 88
  orig: 40, 88
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 1164, 162
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 1164, 116
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: false
  xy: 1124, 68
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 654, 38
  size: 216, 248
  orig: 216, 248
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 1372, 186
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
head s L
  rotate: true
  xy: 536, 2
  size: 48, 24
  orig: 48, 24
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 1270, 118
  size: 20, 12
  orig: 20, 12
  offset: 0, 0
  index: -1
