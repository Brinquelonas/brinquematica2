
mouth 1.png
size: 512,64
format: RGBA8888
filter: Linear,Linear
repeat: none
mouth 1
  rotate: false
  xy: 2, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 2
  rotate: false
  xy: 60, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 3
  rotate: false
  xy: 118, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 4
  rotate: false
  xy: 292, 12
  size: 56, 16
  orig: 56, 16
  offset: 0, 0
  index: -1
mouth 5
  rotate: false
  xy: 176, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 6
  rotate: false
  xy: 234, 2
  size: 56, 44
  orig: 56, 44
  offset: 0, 0
  index: -1
mouth 7
  rotate: false
  xy: 292, 30
  size: 56, 16
  orig: 56, 16
  offset: 0, 0
  index: -1
