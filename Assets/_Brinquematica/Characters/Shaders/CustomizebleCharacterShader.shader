﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Customizable Character" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_DecalTex ("Decals", 2D) = "" {}
		_ShirtColor ("Shirt Color", Color) = (1,0,0,1)
		_ShoesColor ("Shoes Color", Color) = (.2,.2,.2,1)
		_PantsColor ("Pants Color", Color) = (0,.5,1,1)
		_SkinTex ("Skin Base (RGB)", 2D) = "white" { }
		_SkinTone ("Skin Tone", Color) = (0.96,0.72,0.46,1)
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
	}


	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		Pass {
			Name "BASE"
			Cull Off
			//Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _DecalTex;
			sampler2D _SkinTex;
			samplerCUBE _ToonShade;
			float4 _MainTex_ST;
			float4 _DecalTex_ST;
			float4 _SkinTex_ST;
			float4 _Color;
			float4 _ShirtColor;
			float4 _ShoesColor;
			float4 _PantsColor;
			float4 _SkinTone;

			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float3 cubenormal : TEXCOORD1;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.cubenormal = mul (UNITY_MATRIX_MV, float4(v.normal,0));
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, i.texcoord);
				fixed4 decal = tex2D(_DecalTex, i.texcoord);

				fixed4 shirt = _ShirtColor * color.r;
				fixed4 shoes = _ShoesColor * color.g;
				fixed4 pants = _PantsColor * color.b;

				fixed4 skinColor = tex2D(_SkinTex, i.texcoord) * _SkinTone;

				fixed4 col = shirt + shoes + pants + skinColor;
				col.rgb = lerp(col.rgb, decal.rgb, decal.a);
				col *= _Color;

				fixed4 cube = texCUBE(_ToonShade, i.cubenormal) * _Color;

				fixed lum =  dot(col, fixed4(0.2126, 0.7152, 0.0722, 0));

				fixed4 c;

				if (lum > 0.5f)
					c = 1 - (1 - 2 * (col - 0.5f)) * (1 - cube);
				else
					c = (2 * col) * cube;

				return c;
			}
			ENDCG			
		}
	} 

	Fallback "VertexLit"
}
