﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Character Face" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.002, 0.5)) = .005
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_FaceTex ("Face Texture (RGB)", 2D) = "white" { }
		_SkinTone ("Skin Tone", Color) = (0.96,0.72,0.46,1)
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
	}
	
	CGINCLUDE
	#include "UnityCG.cginc"
	
	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f {
		float4 pos : SV_POSITION;
		UNITY_FOG_COORDS(0)
		//fixed4 color : COLOR;
		float2 texcoord : TEXCOORD0;
	};
	
	uniform float _Outline;
	uniform float4 _OutlineColor;

	sampler2D _MainTex;
	float4 _MainTex_ST;
	float4 _Color;
	float4 _SkinTone;
	
	v2f vert(appdata v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);

		float3 norm   = normalize(mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal));
		float2 offset = TransformViewToProjection(norm.xy);

		#ifdef UNITY_Z_0_FAR_FROM_CLIPSPACE //to handle recent standard asset package on older version of unity (before 5.5)
			o.pos.xy += offset * _Outline;
		#else
			o.pos.xy += offset * _Outline;
		#endif

		o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
		//o.color = tex2D(_MainTex, o.texcoord) * _Color * _OutlineColor;

		UNITY_TRANSFER_FOG(o,o.pos);
		return o;
	}
	ENDCG

	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		UsePass "Custom/Customizable Character Face/BASE"
		Pass {
			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite On
			ColorMask RGB
			//Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = _Color * tex2D(_MainTex, i.texcoord) * _SkinTone * _OutlineColor;
			
				UNITY_APPLY_FOG(i.fogCoord, c);
				return col;
				
				//UNITY_APPLY_FOG(i.fogCoord, i.color);
				//return i.color;
			}
			ENDCG
		}
	}
	
	Fallback "Toon/Basic"
}
