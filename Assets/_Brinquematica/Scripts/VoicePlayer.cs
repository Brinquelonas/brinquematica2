﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class VoicePlayer : MonoBehaviour {

    public AudioMixerGroup MixerGroup;

    //private static bool _firstPlay = true;

    private static VoicePlayer _instance;
    public static VoicePlayer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<VoicePlayer>();
            if (_instance == null)
            {
                //_firstPlay = true;
                _instance = new GameObject("VoicePlayer").AddComponent<VoicePlayer>();
            }

            return _instance;
        }
    }

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
            {
                _audioSource = gameObject.AddComponent<AudioSource>();
                _audioSource.spatialBlend = 0;
                _audioSource.outputAudioMixerGroup = MixerGroup;
            }

            return _audioSource;
        }
    }

    /*private IEnumerator WaitForInstantiate(string voice, System.Action callback = null)
    {
        yield return null;
        _firstPlay = false;
        Instance.Play(voice, callback);
    }*/

    public void Play(string voice)
    {
        /*if (_firstPlay)
        {
            StartCoroutine(WaitForInstantiate(voice));
            return;
        }*/

        AudioClip clip = Resources.Load<AudioClip>("Voice/General/" + voice);
        AudioSource.PlayOneShot(clip);
    }

    public void Play(string voice, System.Action callback)
    {
        /*if (_firstPlay)
        {
            StartCoroutine(WaitForInstantiate(voice, callback));
            return;
        }*/

        AudioClip clip = Resources.Load<AudioClip>("Voice/General/" + voice);
        AudioSource.PlayOneShot(clip);
        StartCoroutine(CallCallback(clip.length, callback));
    }

    IEnumerator CallCallback(float time, System.Action callback)
    {
        float t = 0;

        while (t < time)
        {
            t += Time.deltaTime;
            yield return null;
        }

        if (callback != null)
            callback();
    }
}
