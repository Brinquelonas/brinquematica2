﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGamePopup : MonoBehaviour {

    public List<EndGamePopupPlayerPosition> PlayerPositions;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void SetPlayer(int position, PlayerColor color, float score)
    {
        PlayerPositions[position].Color = color;
        PlayerPositions[position].Score = score;
        PlayerPositions[position].Active = true;
    }

	public void SetActive(bool active, System.Action callback = null)
    {
        Tween.Stop();
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Play(() => 
            {
                for (int i = 0; i < PlayerPositions.Count; i++)
                {
                    PlayerPositions[i].SetActive(PlayerPositions[i].Active);
                }

                if (callback != null)
                    callback();
            });
        }
        else
        {
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);

                if (callback != null)
                    callback();
            });
        }
    }

}
