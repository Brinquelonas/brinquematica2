﻿using UnityEngine;
using UnityEngine.Events;
using EasyAR;
using easyar;

public class CanvasTrackableEventHandler : MonoBehaviour
{
    [System.Serializable]
	public class TrackerEvent : UnityEvent { }
	public TrackerEvent OnFound = new TrackerEvent();
	public TrackerEvent OnLost = new TrackerEvent();

    public bool IsFound { get; private set; }
	public bool EnableOnFound = true;
	public bool DisableOnLost = true;

    public ImageTargetBehaviour ImageTarget
    {
        get
        {
            return GetComponent<ImageTargetBehaviour>();
        }
    }

    public ImageTargetController ImageTargetController => GetComponent<ImageTargetController>();

	void Start()
	{
        //ImageTarget.TargetFound += (a) => OnTrackingFound();
        //ImageTarget.TargetLost += (a) => OnTrackingLost();
        ImageTargetController.TargetFound += OnTrackingFound;
        ImageTargetController.TargetLost += OnTrackingLost;

        GetComponent<MeshRenderer>().enabled = false;

        foreach (Transform child in transform)
        {
            if (child != transform)
                child.RotateAround(transform.position, Vector3.right, -90);
        }
	}

	public void OnTrackingFound()
	{
        IsFound = true;
		OnFound.Invoke ();

        if (!EnableOnFound)
            return;

        EnableComponents();
	}


	public void OnTrackingLost()
	{
        IsFound = false;
		OnLost.Invoke ();

        if (!DisableOnLost)
            return;

        DisableComponents();
	}

    public void EnableComponents()
    {
        /*Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
        Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (Renderer component in rendererComponents)
            component.enabled = true;

        foreach (Collider component in colliderComponents)
            component.enabled = true;

        foreach (Canvas component in canvasComponents)
            component.enabled = true;*/
    }

    public void DisableComponents()
    {
        /*Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
        Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (Renderer component in rendererComponents)
            component.enabled = false;

        foreach (Collider component in colliderComponents)
            component.enabled = false;

        foreach (Canvas component in canvasComponents)
            component.enabled = false;*/
    }
}
