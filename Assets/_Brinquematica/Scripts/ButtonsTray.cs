﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsTray : MonoBehaviour {

    public List<Toggle> Toggles;

    public NumberButton SelectedToggle
    {
        get
        {
            for (int i = 0; i < Toggles.Count; i++)
            {
                if (Toggles[i].isOn)
                    return Toggles[i].GetComponent<NumberButton>();
            }
            return null;
        }
    }

    private ToggleGroup _toggleGroup;
	public ToggleGroup ToggleGroup
    {
        get
        {
            if (_toggleGroup == null)
                _toggleGroup = GetComponent<ToggleGroup>();
            return _toggleGroup;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            return _tween;
        }
    }

    private void Awake()
    {
        
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        Tween.Stop();
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Play(callback);
        }
        else
        {
            Tween.Reverse(() => 
            {
                if (callback != null)
                    callback();
                gameObject.SetActive(false);
            });
        }
    }

    public void DeselectAll()
    {
        for (int i = 0; i < Toggles.Count; i++)
        {
            Toggles[i].isOn = false;
        }
    }

}
