﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionsMenu : MonoBehaviour {

    [System.Serializable]
    public struct OperationImage
    {
        public Image Image;
        public Sprite SpriteOn;
        public Sprite SpriteOff;
    }

    public OperationImage AddImage;
    public OperationImage SubImage;
    public OperationImage MulImage;
    public OperationImage DivImage;
    public TextMeshProUGUI LevelTextMesh;

    public PotaTween MenuTween;

    private bool _menuIsOpen;

    public void ButtonClick()
    {
        if (!_menuIsOpen)
            OpenMenu();
        else
            CloseMenu();
    }

    public void OpenMenu()
    {
        _menuIsOpen = true;

        MenuTween.gameObject.SetActive(true);
        MenuTween.Stop();
        MenuTween.Play();

        AddImage.Image.sprite = (GameConfigs.Addition) ? AddImage.SpriteOn : AddImage.SpriteOff;
        SubImage.Image.sprite = (GameConfigs.Subtraction) ? SubImage.SpriteOn : SubImage.SpriteOff;
        MulImage.Image.sprite = (GameConfigs.Multiplication) ? MulImage.SpriteOn : MulImage.SpriteOff;
        DivImage.Image.sprite = (GameConfigs.Division) ? DivImage.SpriteOn : DivImage.SpriteOff;

        LevelTextMesh.text = GameConfigs.Level.ToString();
    }

    public void CloseMenu()
    {
        _menuIsOpen = false;

        MenuTween.Stop();
        MenuTween.Reverse(() => MenuTween.gameObject.SetActive(false));
    }

}
