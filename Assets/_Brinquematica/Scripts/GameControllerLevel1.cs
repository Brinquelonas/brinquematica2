﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Level1Toggles
{
    public List<Toggle> Buttons;
    public List<int> Values;
    public ToggleGroup ToggleGroup;

    public void SetActive(bool active, System.Action callback = null)
    {
        if (active)
        {
            Buttons[0].gameObject.SetActive(true);
            PotaTween.Get(Buttons[0].gameObject).Play(() => 
            {
                Buttons[1].gameObject.SetActive(true);
                PotaTween.Get(Buttons[1].gameObject).Play(() =>
                {
                    Buttons[2].gameObject.SetActive(true);
                    PotaTween.Get(Buttons[2].gameObject).Play(() =>
                    {
                        if (callback != null)
                            callback();
                    });
                });
            });
        }
        else
        {
            PotaTween.Get(Buttons[0].gameObject).Reverse(() =>
            {
                Buttons[0].gameObject.SetActive(false);
            });
            PotaTween.Get(Buttons[1].gameObject).Reverse(() =>
            {
                Buttons[1].gameObject.SetActive(false);
            });
            PotaTween.Get(Buttons[2].gameObject).Reverse(() =>
            {
                Buttons[2].gameObject.SetActive(false);
                if (callback != null)
                    callback();
            });
        }
    }

    public void DeselectAll()
    {
        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].isOn = false;
        }
    }

    public int SelectedValue
    {
        get
        {
            for (int i = 0; i < Buttons.Count; i++)
            {
                if (Buttons[i].isOn)
                {
                    string v = Buttons[i].GetComponent<NumberButton>().Value;
                    int val;

                    if (int.TryParse(v, out val))
                    {
                        return val;
                    }
                    else continue;
                }
            }

            return -1;
        }
    }

    public void SetButtonValues(List<int> values)
    {
        Values = values;

        for (int i = 0; i < Buttons.Count; i++)
        {
            Buttons[i].GetComponentInChildren<Text>().text = values[i].ToString();
            Buttons[i].GetComponent<NumberButton>().Value = values[i].ToString();

            Buttons[i].onValueChanged.RemoveAllListeners();
            Buttons[i].onValueChanged.AddListener((on) => 
            {
                if (on)
                {
                    VoicePlayer.Instance.Play(values[i].ToString());
                }
            });
        }
    }

    public void SetButton(int index, int value)
    {
        if (Values == null || Values.Count != Buttons.Count)
            Values = new List<int>(Buttons.Count);
            
        //Buttons[index].GetComponentInChildren<Text>().text = value.ToString();
        Buttons[index].GetComponent<NumberButton>().Value = value.ToString();
        Values[index] = value;

        Buttons[index].onValueChanged.RemoveAllListeners();
        Buttons[index].onValueChanged.AddListener((on) =>
        {
            if (on)
            {
                VoicePlayer.Instance.Play(Values[index].ToString());
            }
        });
    }
}

public class GameControllerLevel1 : MonoBehaviour {

    public int PlayerIndex { get; set; }
    public int Answer { get; set; }
    public Level1AnswerSlot AnswerSlot;
    public List<Transform> NumberSlots;

    public Level1Toggles Toggles = new Level1Toggles();

    public AnswerFeedback Feedback;
    public Button AnswerButton;
    public Button EndGameButton;
    public Openable EndGameButtonPopup;
    public Button ReturnToMainMenuButton;
    public EndGamePopup EndGamePopup;
    public GameObject IncreaseLevelPopup;

    [Header("Players")]
    public List<PlayerController> Players;
    public List<PlayerController> PlayersInGame { get; set; }

    [Header("UI")]
    public Image Balloon;
    public TextMeshProUGUI Question;

    [Header("Send Mail")]
    public ReportScroll EndGameScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;

    [Header("Audio")]
    public AudioClip PositiveSound;
    public AudioClip NegativeSound;
    public AudioClip EndGameSound;
    public Toggle SoundToggle;

    private GameStep _currentStep = GameStep.None;
    private List<Level1DraggableNumber> _numbers;
    private int _playersFinished;

    private int _min = 1;
    private int _max = 10;
    private static string _report;
    private static List<int> _scores;
    private static List<int> _rightAnswers;
    private static List<int> _wrongAnswers;

    private void Awake()
    {
        if (PlayersConfig.Players == null || PlayersConfig.Players.Count == 0)
        {
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("1", PlayerColor.Blue));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("2", PlayerColor.Green));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("3", PlayerColor.Red));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("4", PlayerColor.Yellow));
        }

        PlayersInGame = new List<PlayerController>();

        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].gameObject.SetActive(false);

            for (int j = 0; j < PlayersConfig.Players.Count; j++)
            {
                if (PlayersConfig.Players[j].Color == Players[i].Color)
                {
                    Players[i].Name = PlayersConfig.Players[j].Name;
                    PlayersInGame.Add(Players[i]);
                }
            }
        }

        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            string cor = "";
            switch (PlayersInGame[i].Color)
            {
                case PlayerColor.Red:
                    cor = "Vermelho";
                    break;
                case PlayerColor.Green:
                    cor = "Verde";
                    break;
                case PlayerColor.Blue:
                    cor = "Azul";
                    break;
                case PlayerColor.Yellow:
                    cor = "Amarelo";
                    break;
                default:
                    break;
            }

            PlayersInGame[i].Log += cor + " - " + PlayersInGame[i].Name + "\n\n";
            PlayersInGame[i].gameObject.SetActive(i == PlayerIndex);
        }

        switch (GameConfigs.Level)
        {
            default:
                break;
            case 0:
                _min = 1;
                _max = 10;
                _report = "";
                _scores = new List<int>();
                _rightAnswers = new List<int>();
                _wrongAnswers = new List<int>();
                for (int i = 0; i < PlayersInGame.Count; i++)
                {
                    _scores.Add(0);
                    _rightAnswers.Add(0);
                    _wrongAnswers.Add(0);
                }
                break;
            case 1:
                _min = 5;
                _max = 16;
                _report += "\n";
                break;
            case 2:
                _min = 10;
                _max = 21;
                _report += "\n";
                break;
        }

        _report += "Nível: 1\n\n";
        _report += "\tDificuldade: " + (GameConfigs.Level + 1) + "\n\n";

        SetQuestion();
    }

    public void SetQuestion()
    {
        _currentStep = GameStep.None;
        EndGameButton.interactable = false;
        if (EndGameButtonPopup.gameObject.activeSelf)
            EndGameButtonPopup.SetActive(false);

        Question.text = "";

        string audioname = "";
        switch (PlayersInGame[PlayerIndex].Color)
        {
            case PlayerColor.Red:
                audioname = "vermelho";
                break;
            case PlayerColor.Green:
                audioname = "verde";
                break;
            case PlayerColor.Blue:
                audioname = "azul";
                break;
            case PlayerColor.Yellow:
                audioname = "amarelo";
                break;
            default:
                break;
        }
        VoicePlayer.Instance.Play(audioname, () => 
        {
            if (SoundToggle.isOn)
                VoicePlayer.Instance.Play("qualnumerobolinhas");
        });

        PlayersInGame[PlayerIndex].FadeIn(() =>
        {
            Question.text = "Qual número representa a quantidade de bolinhas?";

            Answer = Random.Range(_min, _max);

            List<int> answers = new List<int>() { Answer };

            AnswerSlot.SetActive(true, () =>
            {
                AnswerSlot.SetAnswerImage(Answer, () =>
                {
                    int correctButtonIndex = Random.Range(0, Toggles.Buttons.Count);

                    for (int i = 0; i < Toggles.Buttons.Count; i++)
                    {
                        int val;

                        if (i == correctButtonIndex)
                            val = Answer;
                        else
                        {
                            do
                            {
                                val = Random.Range(_min, _max);
                            } while (answers.Contains(val));//(val == Answer);
                            answers.Add(val);
                        }

                        Toggles.SetButton(i, val);
                    }

                    Toggles.SetActive(true, () => 
                    {
                        EndGameButton.interactable = true;
                    });

                });
            });
        });
    }

    private void ShowFeedback()
    {
        _currentStep = GameStep.Answer;

        bool correct = Toggles.SelectedValue == Answer;

        RegisterPlay("Bolinhas: " + Answer + "\n");
        RegisterPlay("Resposta dada: " + Toggles.SelectedValue + "\n");

        if (correct)
        {
            VoicePlayer.Instance.Play("correto");
            Question.text = "Correto!";
            AudioPlayer.Instance.PlayClip(PositiveSound);
            VoicePlayer.Instance.Play("correto");
            RegisterPlay(" Correto!\n\n");
            //PlayersInGame[PlayerIndex].TotalRightAnswers++;
            //PlayersInGame[PlayerIndex].TotalScore++;

            _rightAnswers[PlayerIndex]++;
            _scores[PlayerIndex]++;
        }
        else
        {
            VoicePlayer.Instance.Play("quepena");
            Question.text = "Que pena...";
            RegisterPlay(" Errado.\n\n");
            AudioPlayer.Instance.PlayClip(NegativeSound);
            VoicePlayer.Instance.Play("quepena");
            //PlayersInGame[PlayerIndex].TotalWrongAnswers++;

            _wrongAnswers[PlayerIndex]++;
        }

        Feedback.SetActive(true);
        Feedback.ShowFeedback(correct, () =>
        {
            AnswerButton.interactable = true;
        });
    }

    private void RegisterPlay(string log)
    {
        PlayersInGame[PlayerIndex].Log += (log);
    }

    public void OkButtonClicked()
    {
        switch (_currentStep)
        {
            case GameStep.None:
                if (Toggles.SelectedValue == -1)
                    return;

                EndGameButton.interactable = false;
                if (EndGameButtonPopup.gameObject.activeSelf)
                    EndGameButtonPopup.SetActive(false);

                AnswerSlot.SetActive(false);
                Toggles.SetActive(false, () => 
                {
                    ShowFeedback();
                });
                break;
            case GameStep.Dice:
                break;
            case GameStep.Operation:
                break;
            case GameStep.Space:
                break;
            case GameStep.Answer:
                NextPlayer();
                break;
            case GameStep.EndGame:
                //ReturnToMainMenu();
                OpenIncreaseLevelPopup();
                break;
            case GameStep.Report:
                _currentStep = GameStep.SendMail;
                SendMailPopup.SetActive(true);
                EndGameScroll.SetActive(false);
                break;
            case GameStep.SendMail:
                //SendMailPopup.SetActive(false);
                AnswerButton.gameObject.SetActive(false);

                /*if (SendMailPopup.SendMailButtonClicked(_report))
                {
                    AudioPlayer.Instance.PlayClip(PositiveSound);
                    AlertPopup.Text.text = "E-mail enviado com sucesso!";
                    AlertPopup.SetActive(true);
                    AlertPopup.Button.onClick.AddListener(ReturnToMainMenu);
                    //ReturnToMainMenu();
                }
                else
                {
                    AudioPlayer.Instance.PlayClip(NegativeSound);
                    AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!";
                    AlertPopup.SetActive(true);
                    AlertPopup.Button.onClick.AddListener(() =>
                    {
                        SendMailPopup.SetActive(true);
                        //AnswerButton.gameObject.SetActive(true);
                        AlertPopup.SetActive(false);
                    });
                }*/

                SendMailPopup.SendMailButtonClicked(_report, () => 
                {
                    SendMailPopup.SetActive(false);
                    AudioPlayer.Instance.PlayClip(PositiveSound);
                    AlertPopup.Text.text = "E-mail enviado com sucesso!\nEnviar o relatório para outro e-mail?";
                    AlertPopup.SetActive(true, 1);
                    AlertPopup.Buttons[0].onClick.AddListener(ReturnToMainMenu);
                    AlertPopup.Buttons[1].onClick.AddListener(() =>
                    {
                        AlertPopup.SetActive(false);
                        SendMailPopup.SetActive(true);
                    });
                }, () => 
                {
                    SendMailPopup.SetActive(false);
                    AudioPlayer.Instance.PlayClip(NegativeSound);
                    AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!";
                    AlertPopup.SetActive(true);
                    AlertPopup.Buttons[0].onClick.AddListener(() =>
                    {
                        SendMailPopup.SetActive(true);
                        //AnswerButton.gameObject.SetActive(true);
                        AlertPopup.SetActive(false);
                    });
                });
                break;
            default:
                break;
        }
    }

    public void EndGameButtonClicked()
    {
        /*PlayersInGame[PlayerIndex].FinishedGame = true;
        _playersFinished++;
        //PlayersInGame[PlayerIndex].TotalScore =_playersFinished;

        EndGameButton.interactable = false;

        AnswerSlot.SetActive(false);
        Toggles.SetActive(false, () =>
        {
            ResetAll();
            //NextPlayer();

            //EndGameButton.interactable = true;
        });*/

        EndGameButton.interactable = false;

        AnswerSlot.SetActive(false);
        Toggles.SetActive(false, () =>
        {
            ResetAll();

            EndGame();
        });

        /*VoicePlayer.Instance.Play("finalizarpartida", () => 
        {
            //NextPlayer();
            EndGame();
        });*/
    }

    private void ResetAll()
    {
        /*for (int i = 0; i < _numbers.Count; i++)
        {
            Destroy(_numbers[i].gameObject);
        }*/
        Toggles.DeselectAll();

        Feedback.SetActive(false);
    }

    private void NextPlayer()
    {
        ResetAll();

        if (CheckEndGame())
        {
            EndGame();
            return;
        }

        if (PlayersInGame.Count > 1)
        {
            PlayerIndex++;
            PlayerIndex = PlayerIndex % PlayersInGame.Count;

            if (PlayersInGame[PlayerIndex].FinishedGame)
            {
                NextPlayer();
                return;
            }

            for (int i = 0; i < PlayersInGame.Count; i++)
            {
                if (i != PlayerIndex && PlayersInGame[i].gameObject.activeSelf)
                    PlayersInGame[i].FadeOut();
            }

            SetQuestion();
        }
        else
        {
            SetQuestion();
        }
    }

    private bool CheckEndGame()
    {
        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            if (!PlayersInGame[i].FinishedGame)
                return false;
        }

        return true;
    }

    private void Report()
    {
        _currentStep = GameStep.Report;

        AnswerButton.gameObject.SetActive(false);
        EndGamePopup.SetActive(false);

        EndGameScroll.SetActive(true);

        /*for (int i = 0; i < PlayersInGame.Count; i++)
        {
            PlayersInGame[i].TotalScore = _scores[i];
            PlayersInGame[i].TotalRightAnswers = _rightAnswers[i];
            PlayersInGame[i].TotalWrongAnswers = _wrongAnswers[i];
        }*/

        IEnumerable<PlayerController> players = PlayersInGame.OrderByDescending((p) => p.TotalScore);
        
        EndGameScroll.Text.text = _report;
    }

    private void EndGame()
    {
        _currentStep = GameStep.EndGame;

        ResetAll();

        PlayersInGame[PlayerIndex].FadeOut();

        Balloon.gameObject.SetActive(false);
        //AnswerButton.gameObject.SetActive(false);
        EndGameButton.gameObject.SetActive(false);
        AudioPlayer.Instance.PlayClip(EndGameSound);
        
        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            PlayersInGame[i].TotalScore = _scores[i];
            PlayersInGame[i].TotalRightAnswers = _rightAnswers[i];
            PlayersInGame[i].TotalWrongAnswers = _wrongAnswers[i];
        }

        //List<PlayerController> players = PlayersInGame.OrderBy((p) => p.TotalScore).ToList();
        List<PlayerController> players = PlayersInGame.OrderByDescending((p) => p.TotalScore).ToList();

        for (int i = 0; i < players.Count; i++)
        {
            EndGamePopup.SetPlayer(i, players[i].Color, players[i].TotalScore);
            players[i].Log += "\nRespostas Certas: " + players[i].TotalRightAnswers;
            players[i].Log += "\nRespostas Erradas: " + players[i].TotalWrongAnswers;
            players[i].Log += "\nPontuação Total: " + players[i].TotalScore;
            _report += players[i].Log + "\n-------------------------------------\n\n";
        }

        EndGamePopup.SetActive(true);
    }

    private void OpenIncreaseLevelPopup()
    {
        if (GameConfigs.Level > 1)
        {
            //ReturnToMainMenu();
            DontIncreaseLevel();
            return;
        }

        EndGamePopup.SetActive(false);
        IncreaseLevelPopup.SetActive(true);
        PotaTween.Get(IncreaseLevelPopup).Stop();
        PotaTween.Get(IncreaseLevelPopup).Play();
    }

    public void IncreaseLevel()
    {
        PotaTween.Get(IncreaseLevelPopup).Stop();
        PotaTween.Get(IncreaseLevelPopup).Reverse(() =>
        {
            GameConfigs.Level++;
            SceneManager.LoadScene("GameplayLevel1");
        });
    }

    public void DontIncreaseLevel()
    {
        IncreaseLevelPopup.SetActive(false);
        Report();
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
