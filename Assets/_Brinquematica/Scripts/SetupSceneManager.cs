﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SetupSceneManager : MonoBehaviour
{

    public Toggle AddToggle;
    public Toggle SubtractToggle;
    public Toggle MultiplicationToggle;
    public Toggle DivisionToggle;
    public Toggle Level1Toggle;
    public Toggle Level2Toggle;
    public Toggle Level3Toggle;
    public Toggle Level4Toggle;
    public Toggle TimeToggle;

    public GameObject LoadingText;

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;

            return _audioSource;
        }
    }

    private void Start()
    {
        SetAdd(AddToggle.isOn);
        SetSubtract(SubtractToggle.isOn);
        SetMultiplication(MultiplicationToggle.isOn);
        SetDivision(DivisionToggle.isOn);
        SetLevel1(Level1Toggle.isOn);
        SetLevel2(Level2Toggle.isOn);
        SetLevel3(Level3Toggle.isOn);
        SetLevel4(Level4Toggle.isOn);
        SetTime(TimeToggle.isOn);

        StartCoroutine(PlayAudio());
    }

    public void SetLevel1(bool enable)
    {
        if (enable)
            GameConfigs.Level = 1;
    }

    public void SetLevel2(bool enable)
    {
        if (enable)
            GameConfigs.Level = 2;
    }

    public void SetLevel3(bool enable)
    {
        if (enable)
            GameConfigs.Level = 3;
    }

    public void SetLevel4(bool enable)
    {
        if (enable)
            GameConfigs.Level = 4;
    }

    public void SetAdd(bool enable)
    {
        GameConfigs.Addition = enable;

        if (enable == false && GameConfigs.Subtraction == false && GameConfigs.Multiplication == false && GameConfigs.Division == false)
        {
            AddToggle.isOn = true;
            AddToggle.Select();
        }
    }

    public void SetSubtract(bool enable)
    {
        GameConfigs.Subtraction = enable;

        if (enable == false && GameConfigs.Addition == false && GameConfigs.Multiplication == false && GameConfigs.Division == false)
        {
            SubtractToggle.isOn = true;
            SubtractToggle.Select();
        }
    }

    public void SetMultiplication(bool enable)
    {
        GameConfigs.Multiplication = enable;

        if (enable == false && GameConfigs.Subtraction == false && GameConfigs.Addition == false && GameConfigs.Division == false)
        {
            MultiplicationToggle.isOn = true;
            MultiplicationToggle.Select();
        }
    }

    public void SetDivision(bool enable)
    {
        GameConfigs.Division = enable;

        if (enable == false && GameConfigs.Subtraction == false && GameConfigs.Multiplication == false && GameConfigs.Addition == false)
        {
            DivisionToggle.isOn = true;
            DivisionToggle.Select();
        }
    }

    public void SetTime(bool enable)
    {
        GameConfigs.Time = enable;
    }

    public void Ready()
    {
        //SceneManager.LoadScene("Gameplay");
        StartCoroutine(LoadSceneAsync("Gameplay"));
    }

    IEnumerator PlayAudio()
    {
        yield return new WaitForSeconds(0.5f);

        AudioClip clip = Resources.Load<AudioClip>("Voice/NPC" + Random.Range(1, 7) + "/Config");
        AudioSource.PlayOneShot(clip);
    }

    IEnumerator LoadSceneAsync(string sceneName)
    {
        var async = SceneManager.LoadSceneAsync(sceneName);

        LoadingText.SetActive(true);

        while (!async.isDone)
            yield return null;
    }
}
