﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerFeedback : MonoBehaviour {

    public GameObject PositiveFeedback;
    public GameObject NegativeFeedback;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void ShowFeedback(bool positive, System.Action callback = null)
    {
        PositiveFeedback.SetActive(positive);
        NegativeFeedback.SetActive(!positive);
        
        Tween.Stop();
        Tween.Play(callback);
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            PositiveFeedback.SetActive(false);
            NegativeFeedback.SetActive(false);
            gameObject.SetActive(active);
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
