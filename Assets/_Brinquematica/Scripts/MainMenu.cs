﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Button TableButton;
    public Button ArButton;
    public Button PlayButton;
    public Button QuitButton;

    public PotaTween TableAnim;
    public PotaTween ArAnim;
    public PotaTween PlayAnim;
    public PotaTween Fader;
    public PotaTween Logo;
    public PotaTween QuitButtonAnim;

    public GameObject LoadingText;

    void OnEnable()
    {
        //Invoke("Open", 0.1f);
        Open();
    }

    void Start()
    {
        ArButton.onClick.AddListener(() =>
        {
            Fader.Reverse(() => LoadingText.SetActive(true));
            Logo.Reverse();
            VoicePlayer.Instance.Play("realidadeaumentada", () => 
            {
                //Fader.Reverse(() => SceneManager.LoadScene("ConfigurationScene"));
                //Fader.Reverse(() => StartCoroutine(LoadSceneAsync("ConfigurationScene")));
                StartCoroutine(LoadSceneAsync("ConfigurationScene"));
            });
            ArAnim.Reverse();
            TableAnim.Reverse();
            PlayAnim.Reverse();
            QuitButtonAnim.Reverse();
        });

        TableButton.onClick.AddListener(() =>
        {
            VoicePlayer.Instance.Play("tabuada", () => 
            {
                Fader.gameObject.SetActive(true);
                Fader.Reverse(() => SceneManager.LoadScene("GameplayTable"));
                Logo.Reverse();
            });
            TableAnim.Reverse();
            ArAnim.Reverse();
            PlayAnim.Reverse();
            QuitButtonAnim.Reverse();
        });

        PlayButton.onClick.AddListener(() => 
        {
            VoicePlayer.Instance.Play("jogar", () => 
            {
                Fader.gameObject.SetActive(true);
                Fader.Reverse(() => SceneManager.LoadScene("LevelSelectionScene"));
                Logo.Reverse();
            });
            PlayAnim.Reverse();
            ArAnim.Reverse();
            TableAnim.Reverse();
            QuitButtonAnim.Reverse();
        });

        QuitButton.onClick.AddListener(() => 
        {
            Logo.Reverse();
            PlayAnim.Reverse();
            ArAnim.Reverse();
            TableAnim.Reverse();
            QuitButtonAnim.Reverse();
            Fader.gameObject.SetActive(true);
            Fader.Reverse(() => Application.Quit());
        });
    }

    public void Open()
    {
        Fader.gameObject.SetActive(true);
        Fader.Play(() =>
        {
            Fader.gameObject.SetActive(false);

            Logo.Play(() => 
            {
                VoicePlayer.Instance.Play("brinquematica");

                TableAnim.Play();
                ArAnim.Play();
                PlayAnim.Play();
                QuitButtonAnim.Play();
            });
        });
    }

    IEnumerator LoadSceneAsync(string sceneName)
    {
        var async = SceneManager.LoadSceneAsync(sceneName);

        LoadingText.SetActive(true);

        while (!async.isDone)
            yield return null;
    }
}
