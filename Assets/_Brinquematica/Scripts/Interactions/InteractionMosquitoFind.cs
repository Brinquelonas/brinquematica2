﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionMosquitoFind : Interaction {

    public ModelInteraction MosquitoPrefab;
    public Transform EventsParent;

    private ModelInteraction Mosquito;
    private List<EventSelector> Spaces;

    private GameObject mosquitoPivot;

    private void Start()
    {

    }

    private void OnEnable()
    {
        Spaces = new List<EventSelector>(EventsParent.GetComponentsInChildren<EventSelector>(true));

        int minIndex = 0;//Mathf.Clamp(CurrentSpace - 3, 0, CurrentSpace);
        int maxIndex = Spaces.Count;//Mathf.Clamp(minIndex + 3, CurrentSpace, Spaces.Count - 1);

        int spaceIndex = Random.Range(minIndex, maxIndex);

        while(spaceIndex == CurrentSpace)
            spaceIndex = Random.Range(minIndex, maxIndex);

        Mosquito = Instantiate(MosquitoPrefab, Spaces[spaceIndex].transform.parent);
        Mosquito.OnClickEvent.AddListener(OnMosquitoClick);

        Mosquito.GetComponent<Animator>().SetBool("Animate", true);

        mosquitoPivot = new GameObject("Pivot");
        mosquitoPivot.transform.SetParent(Spaces[spaceIndex].transform.parent);
        mosquitoPivot.transform.localPosition = Vector3.zero;
        Mosquito.transform.SetParent(mosquitoPivot.transform);
        Mosquito.transform.localPosition = new Vector3(30, 10, 0);

        PotaTween tween = PotaTween.Create(mosquitoPivot).
                SetRotation(Vector3.zero, Vector3.up * 360).
                SetDuration(3).
                SetLoop(LoopType.Loop);
        tween.PlayOnEnable = true;
        tween.Play();

        EnableSpaces(false);
    }

    private void OnMosquitoClick(ModelInteraction mosquito)
    {
        Destroy(mosquitoPivot.gameObject);
        EnableSpaces(true);

        OnFinish.Invoke(true);
    }

    private void EnableSpaces(bool enable)
    {
        for (int i = 0; i < Spaces.Count; i++)
            Spaces[i].gameObject.SetActive(enable);
    }
}
