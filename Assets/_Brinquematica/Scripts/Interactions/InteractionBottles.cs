﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionBottles : Interaction {

    public List<Button> Bottles;
    public List<bool> BottlesTurned = new List<bool>();

    private void OnEnable()
    {
        if (BottlesTurned == null || BottlesTurned.Count != Bottles.Count)
        {
            for (int i = 0; i < Bottles.Count; i++)
            {
                InitializeBottle(Bottles[i], i);
                BottlesTurned.Add(false);
            }
        }

        for (int i = 0; i < Bottles.Count; i++)
        {
            PotaTween.Get(Bottles[i].gameObject, "Flip").Reset();
            BottlesTurned[i] = false;
        }
    }

    private void InitializeBottle(Button bottle, int index)
    {
        bottle.onClick.AddListener(() => 
        {
            BottlesTurned[index] = !BottlesTurned[index];
            PotaTween tween = PotaTween.Get(bottle.gameObject, "Flip");
            tween.Stop();
            if (BottlesTurned[index])
                tween.Play(CheckFinish);
            else
                tween.Reverse(CheckFinish);
        });
    }

    private void CheckFinish()
    {
        for (int i = 0; i < BottlesTurned.Count; i++)
        {
            if (BottlesTurned[i] == false)
                return;
        }

        OnFinish.Invoke(true);
    }
}
