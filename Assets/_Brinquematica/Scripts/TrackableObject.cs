﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackableObject : MonoBehaviour {

	protected CanvasTrackableEventHandler _eventHandler;
	public CanvasTrackableEventHandler EventHandler
	{
		get 
		{
			if (_eventHandler == null)
				_eventHandler = transform.root.GetComponent<CanvasTrackableEventHandler> ();
			return _eventHandler;
		}	
	}

	protected virtual void Awake()
	{
		EventHandler.OnFound.AddListener (OnHandlerFound);
		EventHandler.OnLost.AddListener (OnHandlerLost);
	}

	public virtual void OnHandlerFound()
	{

	}

	public virtual void OnHandlerLost()
	{
		
	}
}
