﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour {

    private static Fader _instance;
    public static Fader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Fader>();

            return _instance;
        }
    }

    public List<Sprite> Sprites;
    public Image Image;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void FadeIn(System.Action callback = null)
    {
        Image.sprite = Sprites[Random.Range(0, Sprites.Count)];

        gameObject.SetActive(true);
        Tween.Stop();
        Tween.Reverse(() =>
        {
            gameObject.SetActive(false);

            if (callback != null)
                callback();
        });
    }

    public void FadeOut(System.Action callback = null)
    {
        Image.sprite = Sprites[Random.Range(0, Sprites.Count)];

        gameObject.SetActive(true);
        Tween.Stop();
        Tween.Play(callback);
    }

}
