﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGamePopupPlayerPosition : MonoBehaviour {

    public List<PlayerController> Players;

    public PlayerController CurrentPlayer { get; set; }
    public bool Active { get; set; }

    public PlayerColor Color
    {
        get
        {
            return CurrentPlayer.Color;
        }
        set
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].Color == value)
                {
                    CurrentPlayer = Players[i];
                }
            }

            CurrentPlayer.gameObject.SetActive(true);
        }
    }

    public float Score
    {
        get
        {
            return CurrentPlayer.TotalScore;
        }
        set
        {
            CurrentPlayer.TotalScore = value;
            CurrentPlayer.Name = value.ToString();
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        Tween.Stop();
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Play(callback);
        }
        else
        {
            Tween.Reverse(() =>
            {
                gameObject.SetActive(false);

                if (callback != null)
                    callback();
            });
        }
    }

}
