﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectionSceneManager : MonoBehaviour {

    public List<Toggle> Toggles;

    public int SelectedDifficulty
    {
        get
        {
            for (int i = 0; i < Toggles.Count; i++)
            {
                if (Toggles[i].isOn)
                    return i;
            }

            return -1;
        }
    }

    private void Awake()
    {
        Fader.Instance.FadeIn();

        for (int i = 0; i < Toggles.Count; i++)
        {
            SetupToggles(i, Toggles[i]);
        }
    }

    void Start()
    {
        VoicePlayer.Instance.Play("selecionenivel");
    }

    private void SetupToggles(int index, Toggle toggle)
    {
        toggle.onValueChanged.AddListener((isOn) => 
        {
            if (isOn)
            {
                VoicePlayer.Instance.Play("nivel" + (index + 1));
            }
        });
    }

    public void OkButtonClicked()
    {
        if (SelectedDifficulty < 0)
            return;

        VoicePlayer.Instance.Play("pronto");
        GameConfigs.Level = SelectedDifficulty;

        Fader.Instance.FadeOut(() => 
        {
            SceneManager.LoadScene("ConfigurationScene2");
        });
    }

}
