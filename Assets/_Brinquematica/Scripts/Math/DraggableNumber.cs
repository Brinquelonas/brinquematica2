﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class DraggableNumber : DraggableElement {

    private List<NumberSlot> _collidingSlots = new List<NumberSlot>();

    private int _value;
    public int Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            TextMesh.text = _value.ToString();
        }
    }

    private TextMeshProUGUI _textMesh;
    public TextMeshProUGUI TextMesh
    {
        get
        {
            if (_textMesh == null)
                _textMesh = GetComponentInChildren<TextMeshProUGUI>();

            return _textMesh;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        float distance = Mathf.Infinity;
        NumberSlot slot = null;
        for (int i = 0; i < _collidingSlots.Count; i++)
        {
            float dist = Vector3.Distance(transform.position, _collidingSlots[i].transform.position);
            if (dist < distance)
            {
                slot = _collidingSlots[i];
                distance = dist;
            }
        }

        if (slot != null)
            slot.AssignedNumber = this;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        NumberSlot slot = collision.GetComponent<NumberSlot>();
        if (slot != null)
        {
            _collidingSlots.Add(slot);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        NumberSlot slot = collision.GetComponent<NumberSlot>();
        if (slot != null)
        {
            _collidingSlots.Remove(slot);
        }
    }
}
