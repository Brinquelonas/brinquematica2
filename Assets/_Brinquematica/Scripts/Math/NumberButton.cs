﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class NumberButton : MonoBehaviour
{
    [SerializeField]
    private string _value;
    public string Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            if (TextMesh != null)
                TextMesh.text = _value;
            if (Text != null)
                Text.text = _value;
        }
    }

    private TextMeshProUGUI _textMesh;
    public TextMeshProUGUI TextMesh
    {
        get
        {
            if (_textMesh == null)
                _textMesh = GetComponentInChildren<TextMeshProUGUI>();

            return _textMesh;
        }
    }

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();

            return _text;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }   
}
