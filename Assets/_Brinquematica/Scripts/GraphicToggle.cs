﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicToggle : MonoBehaviour {

    public Toggle ToggleRef;
    public Graphic Graphic;
    public Color OnColor;
    public Color OffColor;

    private void Start()
    {
        Select(ToggleRef.isOn);
        ToggleRef.onValueChanged.AddListener(Select);
    }

    private void Select(bool selected)
    {
        if (selected)
            Graphic.color = OnColor;
        else
            Graphic.color = OffColor;
    }

}
