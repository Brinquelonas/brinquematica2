﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Openable : MonoBehaviour {
    [System.Serializable]
    public class OpenableEvent : UnityEvent { }

    public OpenableEvent OnOpening;
    public OpenableEvent OnOpened;
    public OpenableEvent OnClosing;
    public OpenableEvent OnClosed;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();

            return _tween;
        }
    }

    public void SetActive(bool active)
    {
        Tween.Stop();

        if (active)
        {
            gameObject.SetActive(true);
            OnOpening.Invoke();
            Tween.Play(() => 
            {
                OnOpened.Invoke();
            });
        }
        else
        {
            OnClosing.Invoke();
            Tween.Reverse(() =>
            {
                OnClosed.Invoke();
                gameObject.SetActive(false);
            });
        }
    }
}
