﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using UnityEngine;
using UnityEngine.Events;

public class MailSender {

    [System.Serializable]
    public class MailEvent : UnityEvent { }

    public MailEvent OnSuccess = new MailEvent();
    public MailEvent OnFail = new MailEvent();

    private System.Action _callback;

    /*public static bool SendMail(string toMail, string body)
    {
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("brinquematica@brinquelonas.com.br");
        mail.To.Add(toMail);
        mail.Subject = Application.productName + " " + System.DateTime.Now;

        mail.Body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;

        SmtpClient smtpServer = new SmtpClient("smtp.brinquelonas.com.br");
        smtpServer.Port = 587;
        smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("brinquematica@brinquelonas.com.br", "RealidadeAumentada1");
        smtpServer.EnableSsl = true;
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chai, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
        try
        {
            smtpServer.Send(mail);
        }
        catch(System.Exception e)
        {
            Debug.Log(e.GetBaseException());
            return false;
        }

        return true;
    }*/

    /*public void SendMail(string toMail, string body)
    {
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("brinquematica@brinquelonas.com.br");
        mail.To.Add(toMail);
        mail.Subject = Application.productName + " " + System.DateTime.Now;

        mail.Body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;

        SmtpClient smtpServer = new SmtpClient("smtp.brinquelonas.com.br");
        smtpServer.Port = 587;
        smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("brinquematica@brinquelonas.com.br", "RealidadeAumentada1");
        smtpServer.EnableSsl = true;
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chai, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

        smtpServer.SendCompleted += new SendCompletedEventHandler(SendComplete);
        //smtpServer.SendCompleted += SendComplete;
        smtpServer.SendAsync(mail, "Mail Sent");
    }*/

    public IEnumerator SendMailCoroutine(string toMail, string body)
    {
        yield return null;

        new NativeShare().
            SetSubject(Application.productName + " " + System.DateTime.Now).
            SetText("Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body).
            AddEmailRecipient(toMail).
            SetCallback(SendComplete).
            Share();

        /*MailMessage mail = new MailMessage();

        mail.From = new MailAddress("brinquematica@brinquelonas.com.br");
        mail.To.Add(toMail);
        mail.Subject = Application.productName + " " + System.DateTime.Now;

        mail.Body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;

        SmtpClient smtpServer = new SmtpClient("smtp.brinquelonas.com.br");
        smtpServer.Port = 587;
        smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("brinquematica@brinquelonas.com.br", "RealidadeAumentada1");
        smtpServer.EnableSsl = true;
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chai, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

        bool success = true;
        try
        {
            smtpServer.Send(mail);
        }
        catch (System.Exception e)
        {
            success = false;
            Debug.Log(e.GetBaseException());
            //return false;
            OnFail.Invoke();
        }

        if (success)
        {
            OnSuccess.Invoke();
            yield break;
        }

        //return true;
        yield return null;*/

        
    }

    private void SendComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        string token = (string)e.UserState;
        
        if (e.Cancelled)
            OnFail.Invoke();
        if (e.Error != null)
            OnFail.Invoke();
        else
            OnSuccess.Invoke();
    }

    void SendComplete(NativeShare.ShareResult result, string shareTarget)
    {
        switch(result)
        {
            case NativeShare.ShareResult.NotShared:
            case NativeShare.ShareResult.Unknown:
                OnFail.Invoke();
                break;
            case NativeShare.ShareResult.Shared:
                OnSuccess.Invoke();
                break;
        }
    }

}
