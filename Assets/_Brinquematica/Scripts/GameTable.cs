﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameTable : MonoBehaviour
{
    [System.Serializable]
    public struct Answer
    {
        public TextMeshProUGUI Text;
        public Button Button;
        public int Index;
    }

    public TextMeshProUGUI Equation;
    public Answer[] Answers;
    public PotaTween EquationAnim;
    public PotaTween[] AnswersAnim;
    public PotaTween CorrectFeedback;
    public PotaTween WrongFeedback;
    public Text QuestionsAnsweredText;

    [Header("Name Input")]
    public InputField NameInputField;
    public Button NameOkButton;

    [Header("Send Mail")]
    public ReportScroll EndGameScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;

    [Header("Audio")]
    public AudioClip CorrectAudio;
    public AudioClip WrongAudio;
    public AudioClip EndAudio;
    public Toggle SoundToggle;

    public int CorrectAnswerIndex { get; set; }

    private int _answers;
    private int _correctAnswers;
    private string _log;

    private List<string> _usedQuestions = new List<string>();

    private List<System.TimeSpan> _questionTimes = new List<System.TimeSpan>();
    private System.DateTime _questionStartTime;
    private System.DateTime _questionEndTime;

    private System.TimeSpan _gameTotalTime;
    private System.DateTime _gameStartTime;
    private System.DateTime _gameEndTime;

    void Start ()
    {
        Answers[0].Button.onClick.AddListener(() => ProcessClick(0));
        Answers[1].Button.onClick.AddListener(() => ProcessClick(1));
        Answers[2].Button.onClick.AddListener(() => ProcessClick(2));
        Answers[3].Button.onClick.AddListener(() => ProcessClick(3));

        CorrectFeedback.gameObject.SetActive(false);
        WrongFeedback.gameObject.SetActive(false);

        LockButtons();
        QuestionsAnsweredText.text = "00/15";

        PotaTween.Get(NameInputField.gameObject).Stop();
        PotaTween.Get(NameInputField.gameObject).Play(() => 
        {
            VoicePlayer.Instance.Play("digitenome");
        });

        NameOkButton.onClick.AddListener(() => 
        {
            if (string.IsNullOrEmpty(NameInputField.text))
                return;

            VoicePlayer.Instance.Play("pronto");

            _log += "Tabuada\n\n" + NameInputField.text + "\n\n";
            StartGame();
        });
    }

    void StartGame()
    {
        PotaTween.Get(NameInputField.gameObject).Stop();
        PotaTween.Get(NameInputField.gameObject).Reverse(() => 
        {
            NameInputField.gameObject.SetActive(false);
        });

        Invoke("SortNewQuestion", 0.7f);

        _gameStartTime = System.DateTime.Now;
    }

    void Update ()
    {
		
	}

    public void ProcessClick(int index)
    {
        _log += "Resposta dada: " + Answers[index].Text.text + "\nResposta certa: " + Answers[CorrectAnswerIndex].Text.text + "\n";

        if(CorrectAnswerIndex == index)
        {
            CorrectFeedback.gameObject.SetActive(true);
            CorrectFeedback.Play();
            ReverseAnims(true);
            _log += "Correto!\n";
            _correctAnswers++;
            AudioPlayer.Instance.PlayClip(CorrectAudio);
        }
        else
        {
            WrongFeedback.gameObject.SetActive(true);
            WrongFeedback.Play();
            ReverseAnims(true);
            _log += "Errado...\n";
            AudioPlayer.Instance.PlayClip(WrongAudio);
        }

        Equation.text += " = " + Answers[CorrectAnswerIndex].Text.text;
        _answers++;
        QuestionsAnsweredText.text = _answers.ToString("00") + "/" + GameConfigs.NumberOfQuestions.ToString("00");

        _questionEndTime = System.DateTime.Now;
        System.TimeSpan time = _questionEndTime.Subtract(_questionStartTime);
        _questionTimes.Add(time);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)time.TotalMinutes,
        time.Seconds);

        _log += "Tempo de resposta: " + t + "\n\n";
        //print(_log);

        LockButtons();

        if (_answers < 15)
            Invoke("SortNewQuestion", 2.5f);
        else
        {
            _log += "\nRespostas Corretas: " + _correctAnswers + "\nRespostas Erradas: " + (15 - _correctAnswers);
            EndGame();
        }
    }

    private void EndGame()
    {
        for (int i = 0; i < Answers.Length; i++)
        {
            Answers[i].Button.gameObject.SetActive(false);
        }

        CorrectFeedback.gameObject.SetActive(false);
        WrongFeedback.gameObject.SetActive(false);
        Equation.text = "";

        AudioPlayer.Instance.PlayClip(EndAudio);

        _gameEndTime = System.DateTime.Now;
        _gameTotalTime = _gameEndTime.Subtract(_gameStartTime);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)_gameTotalTime.TotalMinutes,
        _gameTotalTime.Seconds);

        _log += "\nTempo total da atividade: " + t + "\n";

        //SendMailPopup.SetActive(true);
        EndGameScroll.SetActive(true);
        EndGameScroll.Text.text = _log;
    }

    public void SortNewQuestion()
    {
        CorrectFeedback.gameObject.SetActive(false);
        WrongFeedback.gameObject.SetActive(false);

        SetEquationAndAnswers();
        PlayAnims();

        //if (SoundToggle.isOn)
            VoicePlayer.Instance.Play("resolvaoperacao");

        _questionStartTime = System.DateTime.Now;
    }

    public void SetEquationAndAnswers()
    {
        int a = 0;
        int b = 0;

        do
        {
            a = GameConfigs.Multipliers[Random.Range(0, GameConfigs.Multipliers.Count)];// Random.Range(1, 11);
            b = Random.Range(1, 11);
        } while (_usedQuestions.Contains(a + "x" + b) || _usedQuestions.Contains(b + "x" + a));

        int c = a * b;

        _usedQuestions.Add(a + "x" + b);

        int answerIndex = Random.Range(0, 4);
        List<string> wrongAnswers = new List<string>();

        for (int i = 0; i < 4; i++)
        {
            if(i == answerIndex)
            {
                Answers[i].Text.text = c.ToString();
                CorrectAnswerIndex = answerIndex;
            }
            else
            {
                do
                {
                    Answers[i].Text.text = Random.Range(0, 101).ToString();
                } while (Answers[i].Text.text == c.ToString() || wrongAnswers.Contains(Answers[i].Text.text));
                wrongAnswers.Add(Answers[i].Text.text);
            }
        }

        Equation.text = a + " x " + b;

        _log += "Cálculo: " + Equation.text + "\n";
    }

    public void PlayAnims()
    {
        EquationAnim.Play(() => UnlockButtons());

        for (int i = 0; i < 4; i++)
        {
            AnswersAnim[i].Play();
        }
    }

    public void ReverseAnims(bool exceptAnswer)
    {
        if(exceptAnswer)
        {
            for (int i = 0; i < 4; i++)
            {
                if(i != CorrectAnswerIndex)
                    AnswersAnim[i].Reverse();
            }
        }
        else
        {
            EquationAnim.Reverse();

            for (int i = 0; i < 4; i++)
            {
                AnswersAnim[i].Reverse();
            }
        }
    }

    public void LockButtons()
    {
        for (int i = 0; i < 4; i++)
        {
            Answers[i].Button.interactable = false;
        }
    }

    public void UnlockButtons()
    {
        for (int i = 0; i < 4; i++)
        {
            Answers[i].Button.interactable = true;
        }
    }

    public void ReportScrollButtonClicked()
    {
        EndGameScroll.SetActive(false);
        SendMailPopup.SetActive(true);
    }

    public void SendMailButtonClicked()
    {
        /*if (SendMailPopup.SendMailButtonClicked(_log))
            SceneManager.LoadScene("Loader");
        else
        {
            AudioPlayer.Instance.PlayClip(WrongAudio);
        }*/

        //SendMailPopup.SetActive(false);

        /*if (SendMailPopup.SendMailButtonClicked(_log))
        {
            AudioPlayer.Instance.PlayClip(CorrectAudio);
            AlertPopup.Text.text = "E-mail enviado com sucesso!";
            AlertPopup.SetActive(true);
            AlertPopup.Button.onClick.AddListener(() => 
            {
                SceneManager.LoadScene("Loader");
            });
        }
        else
        {
            AudioPlayer.Instance.PlayClip(WrongAudio);
            AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!";
            AlertPopup.SetActive(true);
            AlertPopup.Button.onClick.AddListener(() =>
            {
                SendMailPopup.SetActive(true);
                AlertPopup.SetActive(false);
            });
        }*/

        SendMailPopup.SendMailButtonClicked(_log, () => 
        {
            SendMailPopup.SetActive(false);
            AudioPlayer.Instance.PlayClip(CorrectAudio);
            AlertPopup.Text.text = "E-mail enviado com sucesso!\nEnviar o relatório para outro e-mail?";
            AlertPopup.SetActive(true, 1);
            AlertPopup.Buttons[0].onClick.AddListener(() =>
            {
                SceneManager.LoadScene("MainMenu");
            });
            AlertPopup.Buttons[1].onClick.AddListener(() =>
            {
                AlertPopup.SetActive(false);
                SendMailPopup.SetActive(true);
            });
        }, () => 
        {
            SendMailPopup.SetActive(false);
            AudioPlayer.Instance.PlayClip(WrongAudio);
            AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!";
            AlertPopup.SetActive(true);
            AlertPopup.Buttons[0].onClick.AddListener(() =>
            {
                SendMailPopup.SetActive(true);
                AlertPopup.SetActive(false);
            });
        });
    }
}
