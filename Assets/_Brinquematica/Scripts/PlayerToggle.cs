﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerToggle : MonoBehaviour {

    public InputField NameInputField;
    public PlayerColor Color;

    public string Name
    {
        get
        {
            return NameInputField.text;
        }
    }

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();
            return _toggle;
        }
    }

    private void Awake()
    {
        Toggle.onValueChanged.AddListener((isOn) => 
        {
            NameInputField.gameObject.SetActive(isOn);

            if (isOn)
            {
                string audioname = "";
                switch (Color)
                {
                    case PlayerColor.Red:
                        audioname = "vermelho";
                        break;
                    case PlayerColor.Green:
                        audioname = "verde";
                        break;
                    case PlayerColor.Blue:
                        audioname = "azul";
                        break;
                    case PlayerColor.Yellow:
                        audioname = "amarelo";
                        break;
                    default:
                        break;
                }
                VoicePlayer.Instance.Play(audioname, () => 
                {
                    VoicePlayer.Instance.Play("digitenome");
                });
            }
        });
    }
}
