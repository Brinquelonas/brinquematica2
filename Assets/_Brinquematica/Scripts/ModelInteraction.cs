﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ModelInteraction : MonoBehaviour {

    public enum AnimationTriggerType
    {
        None,
        Trigger,
        Bool
    }

	public Animator Animator;
    public string AnimationKeyName = "Animate";
    public AnimationTriggerType AnimationType = AnimationTriggerType.Bool;

	private bool _animate;

    [System.Serializable]
    public class ModelInteractionEvent : UnityEvent<ModelInteraction> { }
    public ModelInteractionEvent OnClickEvent = new ModelInteractionEvent();

	void OnMouseDown()
	{
		_animate = !_animate;
		Animator.SetBool (AnimationKeyName, _animate);
        OnClickEvent.Invoke(this);
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			OnMouseDown ();
		}
	}
}
