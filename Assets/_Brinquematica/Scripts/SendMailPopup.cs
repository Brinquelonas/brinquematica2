﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendMailPopup : MonoBehaviour
{
    public InputField EmailInputField;
    public GameObject Feedback;

    private MailSender MailSender = new MailSender();

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            return _tween;
        }
    }

    /*public bool SendMailButtonClicked(string body)
    {
        if (string.IsNullOrEmpty(EmailInputField.text))
            return false;

        Feedback.SetActive(true);

        return MailSender.SendMail(EmailInputField.text, body);            
    }*/

    public void SendMailButtonClicked(string body, System.Action successCallback, System.Action failCallback)
    {
        if (string.IsNullOrEmpty(EmailInputField.text))
            return;

        Feedback.SetActive(true);

        MailSender.OnSuccess.RemoveAllListeners();
        MailSender.OnSuccess.AddListener(() =>
        {
            successCallback();
        });

        MailSender.OnFail.RemoveAllListeners();
        MailSender.OnFail.AddListener(() =>
        {
            failCallback();
        });

        //MailSender.SendMail(EmailInputField.text, body);
        StartCoroutine(MailSender.SendMailCoroutine(EmailInputField.text, body));
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        if (active)
        {
            Feedback.SetActive(false);
            gameObject.SetActive(active);
            Tween.Stop();
            Tween.Play(callback);
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);

                if (callback != null)
                    callback();
            });
        }
    }

}
