﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioPlayer : MonoBehaviour {

    public AudioMixerGroup MixerGroup;

    private static AudioPlayer _instance;
    public static AudioPlayer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioPlayer>();
            if (_instance == null)
                _instance = new GameObject("AudioPlayer").AddComponent<AudioPlayer>();

            return _instance;
        }
    }

    private AudioSource _source;
    public AudioSource Source
    {
        get
        {
            if (_source == null)
                _source = GetComponent<AudioSource>();
            if (_source == null)
            {
                _source = gameObject.AddComponent<AudioSource>();
                _source.spatialBlend = 0;
                _source.outputAudioMixerGroup = MixerGroup;
            }

            return _source;
        }
    }

    public void PlayClip(AudioClip clip)
    {
        Source.PlayOneShot(clip);
    }
}
