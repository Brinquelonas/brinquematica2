﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MuteButton : MonoBehaviour {

    public static bool IsMute;

    public AudioMixer AudioMixer;

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            return _toggle;
        }
    }

    void Awake()
    {
        Toggle.isOn = !IsMute;
        AudioMixer.SetFloat("Volume", IsMute ? -80 : 0);

        Toggle.onValueChanged.AddListener((isOn) => 
        {
            IsMute = !isOn;
            AudioMixer.SetFloat("Volume", IsMute ? -80 : 0);
        });
    }
}
