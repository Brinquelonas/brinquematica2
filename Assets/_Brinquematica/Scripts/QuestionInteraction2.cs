﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Spine.Unity;
using TMPro;

public enum GameStep
{
    None,
    Dice,
    Operation,
    Space,
    Answer,
    EndGame,
    Report,
    SendMail
}

public class TeamConfig
{
    public string Name;
    public Color Color;
}

public class QuestionInteraction2 : MonoBehaviour {

    public int PlayerIndex { get; set; }
    public int DiceValue { get; set; }
    public int SpaceLanded { get; set; }
    public MathematicalOperations Operation { get; set; }

    public ButtonsTray DiceValueGameObject;
    public ButtonsTray OperationValueGameObject;
    public ButtonsTray SpaceValueGameObject;
    public EndGamePopup EndGamePopup;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;

    [Header("Players")]
    public List<PlayerController> Players;
    public List<PlayerController> PlayersInGame { get; set; }

    [Header("QI")]
    //public InputField AnswerInputField;
    public NumericKeyboard Keyboard;
    public AnswerFeedback Feedback;
    public Button AnswerButton;
    public Button EndGameButton;
    public Button ReturnToMainMenuButton;
    public ReportScroll EndGameScroll;
    //public Text EndGameText;

	public Image Balloon;
	public TextMeshProUGUI Question;
    public string ResourcesPath = "";

    [Header("Audio")]
    public AudioClip PositiveSound;
    public AudioClip NegativeSound;
    public AudioClip EndGameSound;

    [Header("Test")]
    public bool EnableAdd;
    public bool EnableSubtract;
    public bool EnableMultiplication;
    public bool EnableDivision;

    public int Level;

    private GameStep _currentStep = GameStep.None;
    private NumberButton _selectedNumber;
    private string _report = "";

    public int Space { get; set; }
    public float CorrectAnswer { get; set; }

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;

            return _audioSource;
        }
    }

	protected void Awake()
	{
        if (PlayersConfig.Players == null || PlayersConfig.Players.Count == 0)
        {
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("1", PlayerColor.Blue));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("2", PlayerColor.Green));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("3", PlayerColor.Red));
            PlayersConfig.Players.Add(new PlayersConfig.PlayerData("4", PlayerColor.Yellow));
        }

        PlayersInGame = new List<PlayerController>();

        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].gameObject.SetActive(false);

            for (int j = 0; j < PlayersConfig.Players.Count; j++)
            {
                if (PlayersConfig.Players[j].Color == Players[i].Color)
                {
                    Players[i].Name = PlayersConfig.Players[j].Name;
                    PlayersInGame.Add(Players[i]);
                }
            }
        }

        //PlayersInGame = Players;

        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            string cor = "";
            switch (PlayersInGame[i].Color)
            {
                case PlayerColor.Red:
                    cor = "Vermelho";
                    break;
                case PlayerColor.Green:
                    cor = "Verde";
                    break;
                case PlayerColor.Blue:
                    cor = "Azul";
                    break;
                case PlayerColor.Yellow:
                    cor = "Amarelo";
                    break;
                default:
                    break;
            }

            PlayersInGame[i].Log += cor + " - " + PlayersInGame[i].Name + "\n\n";
            PlayersInGame[i].gameObject.SetActive(i == PlayerIndex);
        }

        SetDiceValueInteraction();

        _report += "Nível: " + (GameConfigs.Level + 1) + "\n\n";
    }

    private void Update()
    {

    }

    private void SetDiceValueInteraction()
    {
        _currentStep = GameStep.Dice;
        Question.text = "";

        PlayersInGame[PlayerIndex].FadeIn(() => 
        {
            DiceValueGameObject.SetActive(true);

            Question.text = "Qual número caiu no dado?";
            VoicePlayer.Instance.Play("qualnumerodado");

            AnswerButton.interactable = true;

            for (int i = 0; i < DiceValueGameObject.Toggles.Count; i++)
            {
                Toggle toggle = DiceValueGameObject.Toggles[i];
                toggle.onValueChanged.RemoveAllListeners();
                toggle.onValueChanged.AddListener((isOn) => 
                {
                if (isOn)
                    VoicePlayer.Instance.Play(toggle.GetComponent<NumberButton>().Value);
                });
            }
        });
    }

    public void SetDiceValue()
    {
        _selectedNumber = DiceValueGameObject.SelectedToggle;

        if (_selectedNumber == null)
            return;

        AnswerButton.interactable = false;

        int diceValue = 0;

        if (int.TryParse(_selectedNumber.Value, out diceValue))
        {
            DiceValue = diceValue;
        }
        else
            return;

        SetOperationInteraction();
    }

    private void SetOperationInteraction()
    {
        _currentStep = GameStep.Operation;
        AnswerButton.interactable = true;

        OperationValueGameObject.SetActive(true);
        DiceValueGameObject.SetActive(false);

        if (GameConfigs.Level == 1)
        {
            OperationValueGameObject.Toggles[2].interactable = false;
            OperationValueGameObject.Toggles[3].interactable = false;
        }

        Question.text = "Qual operação você moveu?";
        VoicePlayer.Instance.Play("qualoperacao");

        for (int i = 0; i < OperationValueGameObject.Toggles.Count; i++)
        {
            Toggle toggle = OperationValueGameObject.Toggles[i];
            toggle.onValueChanged.RemoveAllListeners();
            toggle.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                {
                    string audioname = "";
                    switch (toggle.GetComponent<NumberButton>().Value)
                    {
                        default:
                            break;
                        case "+":
                            audioname = "adicao";
                            break;
                        case "-":
                            audioname = "subtracao";
                            break;
                        case "x":
                            audioname = "multiplicacao";
                            break;
                        case "÷":
                            audioname = "divisao";
                            break;
                    }
                    VoicePlayer.Instance.Play(audioname);
                }
            });
        }
    }

    public void SetOperationValue()
    {
        _selectedNumber = OperationValueGameObject.SelectedToggle;

        if (_selectedNumber == null)
            return;

        AnswerButton.interactable = false;

        string opValue = "";

        opValue = _selectedNumber.Value;

        switch (opValue)
        {
            case "+":
                Operation = MathematicalOperations.Add;
                break;

            case "-":
                Operation = MathematicalOperations.Subtact;
                break;

            case "x":
                Operation = MathematicalOperations.Multiply;
                break;

            case "÷":
                Operation = MathematicalOperations.Divide;
                break;

            default:
                return;
        }

        SetSpaceValueInteraction();
    }

    private void SetSpaceValueInteraction()
    {
        _currentStep = GameStep.Space;
        AnswerButton.interactable = true;

        SpaceValueGameObject.SetActive(true);
        OperationValueGameObject.SetActive(false);

        Question.text = "Em qual casa parou?";
        VoicePlayer.Instance.Play("qualcasa");

        for (int i = 0; i < SpaceValueGameObject.Toggles.Count; i++)
        {
            Toggle toggle = SpaceValueGameObject.Toggles[i];
            toggle.onValueChanged.RemoveAllListeners();
            toggle.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                    VoicePlayer.Instance.Play(toggle.GetComponent<NumberButton>().Value);
            });
        }
    }

    public void SetSpaceValue()
    {
        _selectedNumber = SpaceValueGameObject.SelectedToggle;

        if (_selectedNumber == null)
            return;

        AnswerButton.interactable = false;

        string place = "";

        place = _selectedNumber.Value;

        int value = 0;
        if (int.TryParse(place, out value))
        {
            SpaceLanded = value;
        }
        else if (place == "r")
        {
            ReplaySpace();
            return;
        }
        else if (place == "f")
        {
            NextPlayer();
            return;
        }
        else
            return;

        SetAnswerInteraction();
    }

    private void SetAnswerInteraction()
    {
        _currentStep = GameStep.Answer;
        AnswerButton.interactable = true;

        //AnswerInputField.gameObject.SetActive(true);
        Keyboard.SetActive(true);
        SpaceValueGameObject.SetActive(false);

        SetCalculation();
        VoicePlayer.Instance.Play("resolvaoperacao");
    }

    public void Answer()
    {
        //string answer = AnswerInputField.text;
        string answer = Keyboard.Value;

        if (string.IsNullOrEmpty(answer))
            return;

        AnswerButton.interactable = false;

        StopCoroutine("Timer");
        //AnswerButton.gameObject.SetActive(false);

        RegisterPlay("\n\tRespondido: " + answer);

        print(answer + "   " + CorrectAnswer);

        if (answer.Contains(","))
            answer = answer.Replace(",", ".");

        float a = float.Parse(answer);

        //if (answer == CorrectAnswer.ToString())
        bool correct = Mathf.RoundToInt(a * 1000) == Mathf.RoundToInt(CorrectAnswer * 1000) || answer == CorrectAnswer.ToString();

        if (correct)
        {
            VoicePlayer.Instance.Play("correto");
            Question.text = "Correto!";
            RegisterPlay(" Correto!\n\n");
            AudioSource.PlayOneShot(PositiveSound);
            PlayersInGame[PlayerIndex].TotalRightAnswers++;
            PlayersInGame[PlayerIndex].TotalScore += a;
        }
        else
        {
            VoicePlayer.Instance.Play("quepena");
            Question.text = "Que pena...";
            RegisterPlay(" Errado.\n\n");
            AudioSource.PlayOneShot(NegativeSound);
            PlayersInGame[PlayerIndex].TotalWrongAnswers++;
        }

        RegisterPlay("");

        Keyboard.SetActive(false);

        Feedback.SetActive(true);
        Feedback.ShowFeedback(correct, () =>
        {
            AnswerButton.interactable = true;
        });

        _currentStep = GameStep.None;
    }

    public void OKButtonClicked()
    {
        //AnswerButton.interactable = false;

        switch (_currentStep)
        {
            case GameStep.Dice:
                SetDiceValue();
                break;
            case GameStep.Operation:
                SetOperationValue();
                break;
            case GameStep.Space:
                SetSpaceValue();
                break;
            case GameStep.Answer:
                Answer();
                break;
            case GameStep.EndGame:
                EndGameReport();
                break;
            case GameStep.Report:
                _currentStep = GameStep.SendMail;
                SendMailPopup.SetActive(true);
                EndGameScroll.SetActive(false);
                break;
            case GameStep.SendMail:
                //SendMailPopup.SetActive(false);
                AnswerButton.gameObject.SetActive(false);
                
                SendMailPopup.SendMailButtonClicked(_report, () => 
                {
                    SendMailPopup.SetActive(false);
                    AudioPlayer.Instance.PlayClip(PositiveSound);
                    AlertPopup.Text.text = "E-mail enviado com sucesso!\nEnviar o relatório para outro e-mail?";
                    AlertPopup.SetActive(true, 1);
                    AlertPopup.Buttons[0].onClick.AddListener(ReturnToMainMenu);
                    AlertPopup.Buttons[1].onClick.AddListener(() => 
                    {
                        AlertPopup.SetActive(false);
                        SendMailPopup.SetActive(true);
                    });
                }, () => 
                {
                    SendMailPopup.SetActive(false);
                    AudioPlayer.Instance.PlayClip(NegativeSound);
                    AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!";
                    AlertPopup.SetActive(true);
                    AlertPopup.Buttons[0].onClick.AddListener(() =>
                    {
                        SendMailPopup.SetActive(true);
                        AnswerButton.gameObject.SetActive(true);
                        AlertPopup.SetActive(false);
                    });
                });
                break;
            default:
                NextPlayer();
                return;
        }
    }

    private void ReplaySpace()
    {
        ResetAll();
        SetDiceValueInteraction();
    }

    public void EndGameButtonClicked()
    {
        PlayersInGame[PlayerIndex].FinishedGame = true;
        ResetAll();
        NextPlayer();
    }

    private void EndGame()
    {
        _currentStep = GameStep.EndGame;

        ResetAll();

        PlayersInGame[PlayerIndex].FadeOut();

        Balloon.gameObject.SetActive(false);
        //AnswerButton.gameObject.SetActive(false);
        EndGameButton.gameObject.SetActive(false);

        AudioPlayer.Instance.PlayClip(EndGameSound);

        List<PlayerController> players = PlayersInGame.OrderByDescending((p) => p.TotalScore).ToList();

        for (int i = 0; i < players.Count; i++)
        {
            EndGamePopup.SetPlayer(i, players[i].Color, players[i].TotalScore);
        }

        EndGamePopup.SetActive(true);

        /*IEnumerable<PlayerController> players = PlayersInGame.OrderByDescending((p) => p.TotalScore);

        EndGameScroll.gameObject.SetActive(true);
        EndGameText.text = "";
        foreach (PlayerController player in players)
        {
            player.Log += "\nRespostas Certas: " + player.TotalRightAnswers;
            player.Log += "\nRespostas Erradas: " + player.TotalWrongAnswers;
            player.Log += "\nPontuação Total: " + player.TotalScore;
            EndGameText.text += player.Log + "\n-------------------------------------\n\n";
        }*/

        /*for (int i = 0; i < players.Count; i++)
        {
            players[i].Log += "\nRespostas Certas: " + players[i].TotalRightAnswers;
            players[i].Log += "\nRespostas Erradas: " + players[i].TotalWrongAnswers;
            players[i].Log += "\nPontuação Total: " + players[i].TotalScore;
            EndGameText.text += players[i].Log + "\n-------------------------------------\n\n";
        }*/
    }

    private void EndGameReport()
    {
        _currentStep = GameStep.Report;

        AnswerButton.gameObject.SetActive(false);
        EndGamePopup.SetActive(false);

        IEnumerable<PlayerController> players = PlayersInGame.OrderByDescending((p) => p.TotalScore);

        EndGameScroll.SetActive(true);
        //_report = "";
        foreach (PlayerController player in players)
        {
            player.Log += "\nRespostas Certas: " + player.TotalRightAnswers;
            player.Log += "\nRespostas Erradas: " + player.TotalWrongAnswers;
            player.Log += "\nPontuação Total: " + player.TotalScore;
            _report += player.Log + "\n-------------------------------------\n\n";
        }
        EndGameScroll.Text.text = _report;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void NextPlayer()
    {
        ResetAll();
        //AnswerInputField.gameObject.SetActive(false);

        if (CheckEndGame())
        {
            EndGame();
            return;
        }

        if (PlayersInGame.Count > 1)
        {
            PlayerIndex++;
            PlayerIndex = PlayerIndex % PlayersInGame.Count;

            if (PlayersInGame[PlayerIndex].FinishedGame)
            {
                NextPlayer();
                return;
            }

            for (int i = 0; i < PlayersInGame.Count; i++)
            {
                //PlayersInGame[i].gameObject.SetActive(i == PlayerIndex);
                if (i != PlayerIndex && PlayersInGame[i].gameObject.activeSelf)
                    PlayersInGame[i].FadeOut();
                    //PlayersInGame[i].FadeOut(SetDiceValueInteraction);
            }

            SetDiceValueInteraction();
        }
        else
        {
            SetDiceValueInteraction();
        }

        //SetDiceValueInteraction();
    }

    private bool CheckEndGame()
    {
        for (int i = 0; i < PlayersInGame.Count; i++)
        {
            if (!PlayersInGame[i].FinishedGame)
                return false;
        }

        return true;
    }

    private void SetCalculation()
    {
        float answer = 0;
        string operationText = "";

        switch (Operation)
        {
            case MathematicalOperations.None:
                break;
            case MathematicalOperations.Add:
                answer = SpaceLanded + DiceValue;
                operationText = " + ";
                break;
            case MathematicalOperations.Subtact:
                if (GameConfigs.Level == 1 && SpaceLanded < DiceValue)
                    answer = DiceValue - SpaceLanded;
                else
                    answer = SpaceLanded - DiceValue;
                operationText = " - ";
                break;
            case MathematicalOperations.Multiply:
                answer = SpaceLanded * DiceValue;
                operationText = " x ";
                break;
            case MathematicalOperations.Divide:
                if (SpaceLanded > DiceValue)
                    answer = (float)SpaceLanded / (float)DiceValue;
                else
                    answer = (float)DiceValue / (float)SpaceLanded;
                operationText = " ÷ ";
                break;
            default:
                break;
        }

        CorrectAnswer = answer;
        if (GameConfigs.Level == 1 && SpaceLanded < DiceValue)
            Question.text = DiceValue.ToString() + operationText + SpaceLanded.ToString();
        else
            Question.text = SpaceLanded.ToString() + operationText + DiceValue.ToString();

        RegisterPlay("\tConta: " + Question.text);
        RegisterPlay(" = " + CorrectAnswer.ToString());

        Question.text += " = ?";
    }

    public void OnHandlerFound(int space)
	{
        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        Question.text = "";

        AnswerButton.gameObject.SetActive(true);

                GenerateCalculation();
    }

    #region GenerateOperations
    public void GenerateCalculation()
    {
        List<MathematicalOperations> operations = new List<MathematicalOperations>();
        if (GameConfigs.Addition)
            operations.Add(MathematicalOperations.Add);
        if (GameConfigs.Subtraction)
            operations.Add(MathematicalOperations.Subtact);
        if (GameConfigs.Multiplication)
            operations.Add(MathematicalOperations.Multiply);
        if (GameConfigs.Division)
            operations.Add(MathematicalOperations.Divide);

        int opIndex = Random.Range(0, operations.Count);
        string calculation = "";

        switch (operations[opIndex])
        {
            case MathematicalOperations.Add:
                calculation = GenerateAdd();
                break;
            case MathematicalOperations.Subtact:
                calculation = GenerateSubtract();
                break;
            case MathematicalOperations.Multiply:
                calculation = GenerateMultiplication();
                break;
            case MathematicalOperations.Divide:
                calculation = GenerateDivision();
                break;
            default:
                break;
        }

        Question.text = calculation + " = ?";
    }

    private string GenerateAdd()
    {
        int answer, min, max, number1, number2;

        switch (GameConfigs.AdditionLevel)
        {
            default:
                min = 0;
                max = 0;
                break;
            case 1:
                min = 0;
                max = 9;
                break;
            case 2:
                min = 10;
                max = 20;
                break;
            case 3:
                min = 20;
                max = 99;
                break;
            case 4:
                min = 100;
                max = 999;
                break;
        }

        answer = Random.Range(min, max);
        number1 = Random.Range(min, answer);
        number2 = answer - number1;

        CorrectAnswer = answer;

        return number1 + " + " + number2;
    }

    private string GenerateSubtract()
    {
        int answer, max, number1, number2;
        List<int> subtracts = new List<int>();
        List<int> answers = new List<int>();

        switch (GameConfigs.SubtractionLevel)
        {
            default:
                answers = new List<int>();
                subtracts = new List<int>();
                break;
            case 1:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                subtracts = new List<int>() { 0, 1, 2, 5 };
                break;
            case 2:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                subtracts = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                break;
            case 3:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25 };
                subtracts = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
                break;
            case 4:
                for (int i = 0; i < 100; i++)
                    answers.Add(i);
                for (int i = 50; i < 200; i++)
                    subtracts.Add(i);
                break;
        }

        answer = answers[Random.Range(0, answers.Count)];
        number1 = subtracts[Random.Range(0, subtracts.Count)];
        number2 = answer + number1;

        CorrectAnswer = answer;

        return number2 + " - " + number1;
    }

    private string GenerateMultiplication()
    {
        int answer, number1, number2;

        List<int> multipliers = new List<int>();

        switch (GameConfigs.MultiplicationLevel)
        {
            default:
                number2 = Random.Range(0, 9);
                break;
            case 1:
                multipliers = new List<int>() { 0, 1, 2, 5, 10 };
                number2 = Random.Range(0, 9);
                break;
            case 2:
                multipliers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                number2 = Random.Range(0, 10);
                break;
            case 3:
                multipliers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                number2 = Random.Range(10, 20);
                break;
            case 4:
                multipliers = new List<int>();
                for (int i = 10; i < 99; i++)
                {
                    multipliers.Add(i);
                }
                number2 = Random.Range(20, 99);
                break;
        }

        int index = Random.Range(0, multipliers.Count);
        number1 = multipliers[index];

        answer = number1 * number2;

        CorrectAnswer = answer;

        return number1 + " x " + number2;
    }

    private string GenerateDivision()
    {
        float answer, max, number1, number2;
        List<int> divisors = new List<int>();

        switch (GameConfigs.DivisionLevel)
        {
            default:
                max = 0;
                break;
            case 1:
                max = 9;
                divisors = new List<int>() { 1, 2, 5, 10 };
                break;
            case 2:
                max = 10;
                divisors = new List<int>() { 1, 2, 3, 4, 5, 10 };
                break;
            case 3:
                max = 100;
                divisors = new List<int>() { 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 20 };
                break;
            case 4:
                max = 1000;

                for (int i = 2; i < 50; i++)
                    divisors.Add(i);

                break;
        }

        number2 = divisors[Random.Range(0, divisors.Count)];

        number1 = Random.Range(0, max);
        while (number1 / number2 != Mathf.Round(number1 / number2))
            number1 = Random.Range(0, max);

        answer = number1 / number2;

        CorrectAnswer = (int)answer;

        return number1 + " ÷ " + number2;
    }
    #endregion


    private void ResetAll()
    {
        DiceValueGameObject.DeselectAll();
        DiceValueGameObject.SetActive(false);

        OperationValueGameObject.DeselectAll();
        OperationValueGameObject.SetActive(false);

        SpaceValueGameObject.DeselectAll();
        SpaceValueGameObject.SetActive(false);

        //AnswerInputField.text = "";
        //AnswerInputField.gameObject.SetActive(false);

        //Keyboard.Value = "";
        //Keyboard.Display.text = "?";
        Keyboard.Reset();
        Keyboard.SetActive(false);

        Feedback.SetActive(false);

        //gameObject.SetActive(false);
    }

    private void RegisterPlay(string log)
    {
        PlayersInGame[PlayerIndex].Log += (log);
    }
}
