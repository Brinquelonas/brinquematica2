﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class PlayersConfig
{
    public class PlayerData
    {
        public string Name;
        public PlayerColor Color;
        public string Log;

        public PlayerData(string name, PlayerColor color)
        {
            Name = name;
            Color = color;
            Log = "";
        }
    }

    public static List<PlayerData> Players = new List<PlayerData>();
}

public class PlayerSelectSceneManager : MonoBehaviour {

    public List<PlayerToggle> PlayerToggles;
    public Button ReadyButton;
    public AudioClip ReadySound;

    private void Awake()
    {
        Fader.Instance.FadeIn();
        PlayersConfig.Players = new List<PlayersConfig.PlayerData>();
    }

    private void Start()
    {
        VoicePlayer.Instance.Play("selecionejogadores");
    }

    public void Ready()
    {
        List<PlayersConfig.PlayerData> players = new List<PlayersConfig.PlayerData>();

        for (int i = 0; i < PlayerToggles.Count; i++)
        {
            if (PlayerToggles[i].Toggle.isOn)
            {
                if (!string.IsNullOrEmpty(PlayerToggles[i].Name))
                {
                    //PlayersConfig.Players.Add(new PlayersConfig.PlayerData(PlayerToggles[i].Name, PlayerToggles[i].Color));
                    players.Add(new PlayersConfig.PlayerData(PlayerToggles[i].Name, PlayerToggles[i].Color));
                }
                else
                {
                    return;
                }
            }
        }

        PlayersConfig.Players = players;

        if (PlayersConfig.Players.Count == 0)
            return;

        VoicePlayer.Instance.Play("pronto");
        AudioPlayer.Instance.PlayClip(ReadySound);

        Fader.Instance.FadeOut(() => 
        {
            if (GameConfigs.Level == 0)
                SceneManager.LoadScene("GameplayLevel1");
            else
                SceneManager.LoadScene("Gameplay2");
        });
    }

   
}
