﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReportScroll : MonoBehaviour {

    public Text Text;
    public PotaTween ViewportTween;

    private ScrollRect _scroll;
    public ScrollRect ScrollRect
    {
        get
        {
            if (_scroll == null)
                _scroll = GetComponent<ScrollRect>();

            return _scroll;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            return _tween;
        }
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        if (active)
        {
            gameObject.SetActive(active);
            Tween.Stop();
            Tween.Play(() => 
            {
                ViewportTween.Stop();
                ViewportTween.Play();

                if (callback != null)
                    callback();
            });
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() =>
            {
                gameObject.SetActive(false);

                if (callback != null)
                    callback();
            });
        }
    }
}
