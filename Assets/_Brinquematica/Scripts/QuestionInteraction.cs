﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using TMPro;

public enum MathematicalOperations
{
    None,
    Add,
    Subtact,
    Multiply,
    Divide
}

public static class GameConfigs
{
    public static bool Addition;
    public static bool Subtraction;
    public static bool Multiplication;
    public static bool Division;

    public static bool Time;

    public static int NumberOfQuestions = 15;
    public static List<int> Multipliers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    private static int _level;
    public static int Level
    {
        get { return _level; }
        set
        {
            _level = Mathf.Clamp(value, 0, 4);
        }
    }

    public static int AdditionLevel
    {
        get
        {
            int lvl = Level;

            if (Division)
                lvl = 4;
            else if (Multiplication)
                lvl = Level + 2;
            else if (Subtraction)
                lvl = Level + 1;

            return Mathf.Clamp(lvl, 0, 4);
        }
    }

    public static int SubtractionLevel
    {
        get
        {
            int lvl = Level;

            if (Division)
                lvl = Level + 2;
            if (Multiplication)
                lvl = Level + 1;

            return Mathf.Clamp(lvl, 0, 4);
        }
    }

    public static int MultiplicationLevel
    {
        get
        {
            int lvl = Level;

            if (Division)
                lvl = Level + 1;

            return Mathf.Clamp(lvl, 0, 4);
        }
    }

    public static int DivisionLevel
    {
        get
        {
            return Level;
        }
    }
}

public class QuestionInteraction : MonoBehaviour {

    [System.Serializable]
    public struct InteractionConfig
    {
        public string Tag;
        public Interaction Interaction;
    }

    public List<Transform> NumberPlaces;
    public DraggableNumber NumberPrefab;
    public NumberSlot AnswerSlot;
    public Button AnswerButton;

	public Image Balloon;
	public TextMeshProUGUI Question;
	public TextMeshProUGUI TimerText;
    public List<NPCController> NPCs = new List<NPCController>();
    public string ResourcesPath = "";

    [Header("Test")]
    public bool EnableAdd;
    public bool EnableSubtract;
    public bool EnableMultiplication;
    public bool EnableDivision;

    public int Level;

    private List<DraggableNumber> _numbers = new List<DraggableNumber>();
    private List<NumberSlot> _answerSlots = new List<NumberSlot>();
    private int _npcIndex;

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }

    private NPCController CurrentNPC
    {
        get
        {
            return NPCs[_npcIndex];
        }
    }

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;

            return _audioSource;
        }
    }

	protected void Awake()
	{

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            OnHandlerFound(0);
        }
    }

    public void OnHandlerFound(int space)
	{
        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        Question.text = "";

        /*GameConfigs.Addition = EnableAdd;
        GameConfigs.Subtraction = EnableSubtract;
        GameConfigs.Multiplication = EnableMultiplication;
        GameConfigs.Division = EnableDivision;
        GameConfigs.Level = Level;*/

        _npcIndex = Random.Range(0, NPCs.Count);

        AnswerButton.gameObject.SetActive(true);
        CurrentNPC.gameObject.SetActive(true);
        CurrentNPC.PlayAnimationLoop("waiting");
        CurrentNPC.PlayMouthAnimation("waiting");

        ShowBalloon(() => 
            {
                CreateNumbers();
                GenerateCalculation();
                CreateSlots();

                if (GameConfigs.Time)
                    StartCoroutine("Timer");

                CurrentNPC.PlayAnimationLoop("question");
                PlayAudio(Resources.Load<AudioClip>("Voice/NPC" + (_npcIndex + 1) + "/Question"), () =>
                {
                    CurrentNPC.PlayAnimationLoop("waiting");
                    CurrentNPC.PlayMouthAnimation("waiting");
                });
            });
    }

    public void GenerateCalculation()
    {
        List<MathematicalOperations> operations = new List<MathematicalOperations>();
        if (GameConfigs.Addition)
            operations.Add(MathematicalOperations.Add);
        if (GameConfigs.Subtraction)
            operations.Add(MathematicalOperations.Subtact);
        if (GameConfigs.Multiplication)
            operations.Add(MathematicalOperations.Multiply);
        if (GameConfigs.Division)
            operations.Add(MathematicalOperations.Divide);

        int opIndex = Random.Range(0, operations.Count);
        string calculation = "";

        switch (operations[opIndex])
        {
            case MathematicalOperations.Add:
                calculation = GenerateAdd();
                break;
            case MathematicalOperations.Subtact:
                calculation = GenerateSubtract();
                break;
            case MathematicalOperations.Multiply:
                calculation = GenerateMultiplication();
                break;
            case MathematicalOperations.Divide:
                calculation = GenerateDivision();
                break;
            default:
                break;
        }

        Question.text = calculation + " = ?";
    }

    private string GenerateAdd()
    {
        int answer, min, max, number1, number2;

        switch (GameConfigs.AdditionLevel)
        {
            default:
                min = 0;
                max = 0;
                break;
            case 1:
                min = 0;
                max = 9;
                break;
            case 2:
                min = 10;
                max = 20;
                break;
            case 3:
                min = 20;
                max = 99;
                break;
            case 4:
                min = 100;
                max = 999;
                break;
        }

        answer = Random.Range(min, max);
        number1 = Random.Range(min, answer);
        number2 = answer - number1;

        CorrectAnswer = answer;

        return number1 + " + " + number2;
    }

    private string GenerateSubtract()
    {
        int answer, max, number1, number2;
        List<int> subtracts = new List<int>();
        List<int> answers = new List<int>();

        switch (GameConfigs.SubtractionLevel)
        {
            default:
                answers = new List<int>();
                subtracts = new List<int>();
                break;
            case 1:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                subtracts = new List<int>() { 0, 1, 2, 5 };
                break;
            case 2:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                subtracts = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                break;
            case 3:
                answers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25 };
                subtracts = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
                break;
            case 4:
                for (int i = 0; i < 100; i++)
                    answers.Add(i);
                for (int i = 50; i < 200; i++)
                    subtracts.Add(i);
                break;
        }

        answer = answers[Random.Range(0, answers.Count)];
        number1 = subtracts[Random.Range(0, subtracts.Count)];
        number2 = answer + number1;

        CorrectAnswer = answer;

        return number2 + " - " + number1;
    }

    private string GenerateMultiplication()
    {
        int answer, number1, number2;

        List<int> multipliers = new List<int>();

        switch (GameConfigs.MultiplicationLevel)
        {
            default:
                number2 = Random.Range(0, 9);
                break;
            case 1:
                multipliers = new List<int>() { 0, 1, 2, 5, 10 };
                number2 = Random.Range(0, 9);
                break;
            case 2:
                multipliers = new List<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                number2 = Random.Range(0, 10);
                break;
            case 3:
                multipliers = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                number2 = Random.Range(10, 20);
                break;
            case 4:
                multipliers = new List<int>();
                for (int i = 10; i < 99; i++)
                {
                    multipliers.Add(i);
                }
                number2 = Random.Range(20, 99);
                break;
        }

        int index = Random.Range(0, multipliers.Count);
        number1 = multipliers[index];

        answer = number1 * number2;

        CorrectAnswer = answer;

        return number1 + " x " + number2;
    }

    private string GenerateDivision()
    {
        float answer, max, number1, number2;
        List<int> divisors = new List<int>();

        switch (GameConfigs.DivisionLevel)
        {
            default:
                max = 0;
                break;
            case 1:
                max = 9;
                divisors = new List<int>() { 1, 2, 5, 10 };
                break;
            case 2:
                max = 10;
                divisors = new List<int>() { 1, 2, 3, 4, 5, 10 };
                break;
            case 3:
                max = 100;
                divisors = new List<int>() { 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 20 };
                break;
            case 4:
                max = 1000;

                for (int i = 2; i < 50; i++)
                    divisors.Add(i);

                break;
        }

        number2 = divisors[Random.Range(0, divisors.Count)];

        number1 = Random.Range(0, max);
        while (number1 / number2 != Mathf.Round(number1 / number2))
            number1 = Random.Range(0, max);

        answer = number1 / number2;

        CorrectAnswer = (int)answer;

        return number1 + " ÷ " + number2;
    }

    public void Answer()
    {
        StopCoroutine("Timer");
        AnswerButton.gameObject.SetActive(false);

        string answer = "";
        for (int i = 0; i < _answerSlots.Count; i++)
        {
            if(_answerSlots[i].gameObject.activeSelf)
            {
                if (_answerSlots[i].AssignedNumber != null)
                    answer += _answerSlots[i].AssignedNumber.Value.ToString();
            }
        }

        print(answer + "   " + CorrectAnswer);

        if (answer == CorrectAnswer.ToString())
        {
            Question.text = "Correto!\nJogue novamente!";
            CurrentNPC.PlayAnimation("right");
            CurrentNPC.PlayMouthAnimation("right");
            PlayAudio(Resources.Load<AudioClip>("Voice/NPC" + (_npcIndex + 1) + "/Right"), ResetAll);
            //Hide();
        }
        else
        {
            Question.text = "Que pena...\nNão jogue novamente.";
            CurrentNPC.PlayAnimation("wrong");
            CurrentNPC.PlayMouthAnimation("wrong");
            PlayAudio(Resources.Load<AudioClip>("Voice/NPC" + (_npcIndex + 1) + "/Wrong"), ResetAll);
            //Hide();
            //ResetSlots();
        }        
    }
    
    private void CreateNumbers()
    {
        if (_numbers.Count > 0)
            return;

        for (int i = 0; i < NumberPlaces.Count; i++)
        {
            DraggableNumber number = Instantiate(NumberPrefab, NumberPlaces[i]);
            number.gameObject.SetActive(true);
            number.transform.localPosition = Vector3.zero;
            number.Value = i;
            _numbers.Add(number);

            number.OnEnd.AddListener((n) => 
            {
                number.transform.position = number.StartPosition;
            });
        }

        NumberPrefab.gameObject.SetActive(false);
    }

    private void CreateSlots()
    {
        for (int i = 0; i < _answerSlots.Count; i++)
            Destroy(_answerSlots[i].gameObject);
        _answerSlots = new List<NumberSlot>();

        AnswerSlot.gameObject.SetActive(true);
        int numberOfSlots = CorrectAnswer.ToString().Length;

        for (int i = 0; i < numberOfSlots; i++)
        {
            NumberSlot slot = Instantiate(AnswerSlot, transform);
            slot.gameObject.SetActive(true);
            Vector3 pos = slot.GetComponent<RectTransform>().anchoredPosition;
            pos.x = 250 * i - 250 * (numberOfSlots - 1) * 0.5f;
            slot.GetComponent<RectTransform>().anchoredPosition = pos;

            _answerSlots.Add(slot);
        }
        AnswerSlot.gameObject.SetActive(false);
    }

    private void ResetSlots()
    {
        for (int i = 0; i < _answerSlots.Count; i++)
        {
            _answerSlots[i].AssignedNumber = null;
        }
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            CurrentNPC.PlayMouthAnimation(2/*, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));*/);
            return;
        }

        //AudioSource.PlayClipAtPoint(clip, Vector3.zero);
        AudioSource.PlayOneShot(clip);
        CurrentNPC.PlayMouthAnimation(clip.length/*, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));*/);

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void PlaySpineAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentNPC.PlayAnimation(animationName, loop, callback);
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }

    private void DisableInteraction(Interaction interaction)
    {
        HideElement(interaction.gameObject, () =>
        {
            interaction.gameObject.SetActive(false);
        });
    }

    private void Hide()
    {   
        StartCoroutine(HideCoroutine());
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator Timer()
    {
        float elapsedTime = 0;

        while(elapsedTime < 25)
        {
            elapsedTime += Time.deltaTime;
            TimerText.text = (Mathf.CeilToInt(25 - elapsedTime)).ToString();

            yield return null;
        }

        Answer();
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }

    private IEnumerator HideCoroutine()
    {
        yield return new WaitForSeconds(3f);

        CurrentNPC.gameObject.SetActive(false);

        for (int i = 0; i < _numbers.Count; i++)
            Destroy(_numbers[i].gameObject);
        _numbers = new List<DraggableNumber>();

        for (int i = 0; i < _answerSlots.Count; i++)
            Destroy(_answerSlots[i].gameObject);
        _answerSlots = new List<NumberSlot>();

        gameObject.SetActive(false);
    }

    private void ResetAll()
    {
        CurrentNPC.gameObject.SetActive(false);

        for (int i = 0; i < _numbers.Count; i++)
            Destroy(_numbers[i].gameObject);
        _numbers = new List<DraggableNumber>();

        for (int i = 0; i < _answerSlots.Count; i++)
            Destroy(_answerSlots[i].gameObject);
        _answerSlots = new List<NumberSlot>();

        gameObject.SetActive(false);
    }
}
