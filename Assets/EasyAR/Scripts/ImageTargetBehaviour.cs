//=============================================================================================================================
//
// Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//=============================================================================================================================
using easyar;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace EasyAR
{
    public class ImageTargetBehaviour : MonoBehaviour// ImageTargetBaseBehaviour
    {
        public string Path;
        public string Name;
        public Vector2 Size;

        public event Action TargetFound;
        public event Action TargetLost;

        private ImageTargetController controller;
        private Vector2 size;

        protected /*override*/ void Awake()
        {
            size = Size;

            if (controller == null)
                controller = GetComponent<ImageTargetController>();
            if (controller == null)
                controller = gameObject.AddComponent<ImageTargetController>();

            controller.ImageFileSource.Scale = size.x;
            controller.ImageFileSource.Path = Path;
            controller.Tracker = FindObjectOfType<ImageTrackerFrameFilter>();

            controller.TargetFound += TargetFound;
            controller.TargetLost += TargetLost;
        }
    }    
}
